package src.inputoutput;
import java.util.HashSet;
import java.util.Set;

import src.countries.Canada;
import src.countries.State;
import src.parties.Party;
/**
 * Class that Reads Input for Canada.
 */
public class ReadInputCanada extends ReadInput {
    /**
     * Creates a ReadInput object for Canada
     * @param filename : String
     */
    public ReadInputCanada(String filename) {
        super(filename);
    }

    /**
     * Creates States depending on the Set<Party> that is being inputted
     * @param allParties : Set<Party>
     * @return : Set<State>
     */
    @Override
    public Set<State> createStates(Set<Party> allParties){
        final int StatesLocationInFile = 2;
        Set<State> states = new HashSet<State>();
        var uniqueProvinces = GetUniqueData(StatesLocationInFile);
 
        for (String provinceName : uniqueProvinces) {
            int seat = 0;
            // Looping through Canada Provinces from Canada Object to get their
            // corresponding seats.
            for (var province : Canada.allStates.values()) {
                if (provinceName.equals(province.getName().toLowerCase())) {
                    seat = province.getSeats();
                    break;
                } else {
                    // do nothing
                }
            }
            var parties = cloneParties(allParties);
            states.add(new State(provinceName, seat, parties));
        }
        return states;
    }

}