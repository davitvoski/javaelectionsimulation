package src.inputoutput;
import java.io.File;
import java.io.IOException;

import src.countries.Country;
import src.parties.Parties;

import java.io.FileWriter;


public class WriteOutput {
    Country country;
    Parties parties;
    String fileName;
    /**
     * Creates a WriteOuput object that will be used to create a randomly generated file
     * with the correct country, parties, filename that will be read from with the readinput object
     * @param country : Country
     * @param parties : Parties
     * @param fileName : String
     */
    public WriteOutput(Country country, Parties parties, String fileName){
        this.country = country;
        this.parties = parties;
        this.fileName = fileName;
    }
    /**
     * Deletes the file if the filename already exists if not it doe nothing
     */
    public void deleteFile(){
        File newFile = new File(this.fileName);
        if (newFile.delete()){
            System.out.println("File "+this.fileName+" has been successfully deleted");
        }else{
            System.out.println("File does not exist yet");
        }
    }
    /**
     * Creates a file that will be written in with the generated random vote method
     */
    public void createFile(){
        File newFile = new File(this.fileName);
        try {
            if (newFile.createNewFile()){
                System.out.println("File Created");
            }else{
                System.out.println("File already exists");
            }
        } catch (IOException e) {
            System.out.println("An error has occured while creating the file " + e);
            e.printStackTrace();
        }
    }
    /**
     * Generates random lines depending on how many people are in the country 
     * it will write firstly the name of a person, then the party they vote for and where they are from
     * EX: Raphael C,Liberal, Quebec
     */
    public void generateRandomVote(){
        try {
            FileWriter fileWriter = new FileWriter(this.fileName);
            int count = 0;
            while (count < this.country.getPopulation()) {
                String person = "Person "+(count+1);
                String randParty = this.parties.getRandomParty().getPartyName();
                String randState = this.country.getRandomState().getName();
                String line = person+","+randParty+","+randState+"\n";
                fileWriter.write(line);
                count++;
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error has occured while writing to the file" + e);
            e.printStackTrace();
        }    
    }
}
