package src.inputoutput;
import java.util.HashSet;
import java.util.Set;

import src.countries.Mexico;
import src.countries.State;
import src.parties.Party;
/**
 * Class that Reads Input for USA.
 */
public class ReadInputMexico extends ReadInput {
    /**
     * Creates a ReadInput object for mexico
     * @param filename : String
     */
    public ReadInputMexico(String filename) {
        super(filename);
    }
    /**
     * Creates States depending on the Set<Party> that is being inputted
     * @param allParties : Set<Party>
     * @return : Set<State>
     */
    @Override
    public Set<State> createStates(Set<Party> allParties){
        final int StatesLocationInFile = 2;
        Set<State> states = new HashSet<State>();
        var uniqueProvinces = GetUniqueData(StatesLocationInFile);
 
        for (String stateName : uniqueProvinces) {
            int seat = 0;
            // Looping through Canada Provinces from Canada Object to get their
            // corresponding seats.
            for (var state : Mexico.allStates.values()) {
                if (stateName.equals(state.getName().toLowerCase())) {
                    seat = state.getSeats();
                    break;
                } else {
                    // do nothing
                }
            }
            var parties = cloneParties(allParties);
            states.add(new State(stateName, seat, parties));
        }
        return states;
    }

}
