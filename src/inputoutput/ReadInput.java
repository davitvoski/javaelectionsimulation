package src.inputoutput;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import src.countries.State;
import src.parties.Party;
import src.countries.Person;

/**
 * Class that Reads Input generally for a country.
 */
public abstract class ReadInput {
    private String filename;
    protected List<String[]> data;

    public ReadInput(String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("File is null, Input a valid FileName");
        }
        this.filename = filename;
        this.data = new ArrayList<String[]>();
    }

    /**
     * Creates the Unique States according to data given in the file. The method
     * Uses the method GetUniqueData(Path p).
     * This method accepts a list of allParties.
     * @param allParties : List of all parties in a country 
     * @return : List of States.
     */
    public abstract Set<State> createStates(Set<Party> allParties);
    
    /**
     * This method reads the file and populates the data variable 
     * with the data in the file.
     */
    public void readFile(){
        try {
            var p = Paths.get(this.filename);
            // p = Paths.get(fileName);
            var lines = Files.readAllLines(p);
            for (String line : lines) {
                var pieces = line.split(",");
                this.data.add(pieces);
            }
        } catch (IOException e) {
            System.out.println("Failed to Read FIle");
            e.printStackTrace();
        }
    }


    /**
     * Gets the data from the file and creates all the people according to the file
     * provided.
     * 
     * @return : List<Person> a list of people.
     */
    public List<Person> createPeople() {
        List<Person> people = new ArrayList<Person>();


            for (String[] pieces : this.data) {

                if (pieces[0] == " " || pieces[0] == " " || pieces[1] == "" || pieces[1] == " " || pieces[2] == ""
                        || pieces[2] == " ") {
                    throw new IllegalArgumentException("Fields are/is empty or contains just a space.");
                }
                try {
                    people.add(new Person(pieces[0], pieces[2],pieces[1] ));

                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println(
                            "File doesn't meet the necessary requirements of 3 fields (Name,Party,State) per line.");
                    System.out.println("Potential Causes: \n 1.File has an empty line.\n 2.The 3 fields do not exists");
                }
            }
        return people;
    }
    /**
     * Creates a set<Party> that will be used for the country that is being read from
     * @return Set<Party> : Set of Parties in the file
     */
    public Set<Party> createParties(){
        // The Data in the file for a Party must be the second column.
        final int PartiesLocationInFile = 1;
        Set<Party> parties = new HashSet<Party>();

        var uniqueParties = GetUniqueData(PartiesLocationInFile);

        for (String partyName : uniqueParties) {
            parties.add(new Party(partyName));
        }
        return parties;
    }

    /**
     * This method checks for duplicate states, delete them and returns a unique
     * data set of lines of unique provinces.
     * 
     * @param dataPosition : Position of the unique data we want
     * @return : Set of Strings of Unique data
     */
    protected Set<String> GetUniqueData(int dataPostion){
        // This list uniqueData To have only unique provinces.
        Set<String> uniqueData = new HashSet<String>();

        for (String[] line : this.data) {
            var datas = line[dataPostion].toLowerCase();
            uniqueData.add(datas);
        }
        return uniqueData;
    }
    
    /**
     * This method deep clones a list of parties to a new list with different references.
     * @param allParties  : List of Parties.
     * @return : Copy of a List of Parties.
     */
    protected Set<Party> cloneParties(Set<Party> allParties){
        Set<Party> partiesClone = new HashSet<Party>();

        for (Party party : allParties) {
            partiesClone.add(new Party(party.getPartyName()));
        }
        return partiesClone;
    }
}
