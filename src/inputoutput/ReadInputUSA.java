package src.inputoutput;
import java.util.HashSet;
import java.util.Set;

import src.countries.State;
import src.countries.USA;
import src.parties.Party;
/**
 * Class that Reads Input for USA.
 */
public class ReadInputUSA extends ReadInput {
    /**
     * Creates a ReadInput object for USA
     * @param filename : String
     */
    public ReadInputUSA(String filename) {
        super(filename);
    }
    /**
     * Creates States depending on the Set<Party> that is being inputted
     * @param allParties : Set<Party>
     * @return : Set<State>
     */
    @Override
    public Set<State> createStates(Set<Party> allParties){
        final int StatesLocationInFile = 2;
        Set<State> states = new HashSet<State>();
        var uniqueStates = GetUniqueData(StatesLocationInFile);

        for (String stateName : uniqueStates) {
            int seat = 0;
            // Looping through US States from USA Object to get their corresponding seats.
            for (var state : USA.allStates.values()) {
                if (stateName.equals(state.getName().toLowerCase())) {
                    seat = state.getSeats();
                    break;
                } else {
                    // do nothing
                }
            }
            Set<Party> parties = cloneParties(allParties);
            states.add(new State(stateName, seat, parties));
        }
        return states;
    }

}

