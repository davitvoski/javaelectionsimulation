package src.systems;
import java.util.List;
import java.util.Set;

import src.countries.Person;
import src.parties.Party;
/**
 * Class that Implements ISystems.
 * It is an Electoral System where its only function is to calculate
 * the wining party based on the MajorityVote rules.
 */
public class MajorityVote implements ISystems{
    List<Person> population;
    Set<Party> allParties;
    /**
     * Creates a majority vote object that will be used to calculate the winner with the most otes
     * @param population : population of country
     * @param allParties : all the parties for a country
     */
    public MajorityVote(List<Person> population,Set<Party> allParties){
        this.population = population;
        this.allParties = allParties;
    }
    /**
     * Calculates the winner depending on who got the most votes 
     * @return Party
     */
    @Override
    public Party calculateWinner() {
        Party partyWinner = null;
        int previousHighestVotes=0;
        calculatePartyVotes();

        for (Party party : allParties) {
            if (previousHighestVotes < party.getVote()) {
                previousHighestVotes = party.getVote();
                partyWinner = party;
            }
        }
        //calculates the winner and returns the party that won
        return partyWinner;
    }

     /**
     * This method calculate the votes for each party in each state.
     */
    private void calculatePartyVotes() {
        for (Person person : this.population) {
            //Getting Person Party
            var personParty = person.getVotedParty();

            //Checking which party the person voted for 
            for (Party party : this.allParties) {
                if(personParty.toLowerCase().equals(party.getPartyName().toLowerCase())){
                    //Incremening the vote of the voted party
                    party.incVote();
                }
            }
        }
    }
}
