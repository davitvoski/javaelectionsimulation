package src.systems;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import src.countries.Person;
import src.countries.State;
import src.parties.Party;

public class SeatBasedUsa implements ISystems {
    private List<Person> population;
    private Set<State> states;
    
    public SeatBasedUsa(List<Person> population, Set<State> states){
        this.population = population;
        this.states = states;
    }
    /**
     * Calculates a winner depending on who got the most seats won
     * @return Party
     */
    @Override
    public Party calculateWinner() { 
        Party partyWinner = null;
        int previousSeatWinner=0;
        List<Party> allStatesPartyWinners = new ArrayList<Party>();
        
        //Adding the votes of the parties that the people voted for per state to the
        //Corresponding Party.
        calculatePartyVotes();

        //Getting the party winner for each state.
        calculatePartyWinnerPerState(allStatesPartyWinners);
        

        //Tallying all the seats per party.
        allStatesPartyWinners = rallyPartySeats(allStatesPartyWinners);

        //Checking which party has the most seats.
        for (Party party : allStatesPartyWinners) {
            if(previousSeatWinner < party.getSeat()){
                previousSeatWinner = party.getSeat();
                partyWinner = party;
            }
        }  
        return partyWinner;
    }

    /**
     * This method calculates the winning Party in each state based on the number of votes.
     */
    private void calculatePartyWinnerPerState(List<Party> allStatesPartyWinners) {
        Party stateWinner = null;
        for (State state : this.states) {
            int winningVotes = 0;
            var stateParties = state.getAllParties();
            for (Party party : stateParties) {
                if(winningVotes < party.getVote()){
                    winningVotes = party.getVote();
                    stateWinner = party;
                }else{
                    //do nothing
                }
            }
            stateWinner.setSeat(state.getSeat());
            allStatesPartyWinners.add(stateWinner); //Add
        }
    }

    /**
     * This method calculate the votes for each party in each state.
     */
    private void calculatePartyVotes() {
        //Getting Person State and Person Party
        for (Person person : this.population) {
            var personState = person.getStateName();
            var personParty = person.getVotedParty();

            //Checking which State did the person belong to
            for (State state : this.states) {
                if(personState.toLowerCase().equals(state.getName().toLowerCase())){
                    var stateParties = state.getAllParties();

                    //Checking which party the person voted for
                    // System.out.println("State is: " + state.getName()); 
                    for (Party party : stateParties) {
                        if(personParty.toLowerCase().equals(party.getPartyName().toLowerCase())){
                            // System.out.println("Party is: " + party.getPartyName());
                            //Incrementing the votes of the voted party
                            party.incVote();
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Thie method tallies up (totals) all the seats won by a party.
     * @param allStatePartyWinners : List of party winnerr
     * @return : List of parties with tallied seats
     */
    private List<Party> rallyPartySeats(List<Party> allStatePartyWinners){
        List<Party> allParties;
        Map<String, Party> mapParties  = new HashMap<String,Party>();
        //Totaling all the seats of each party
        for (Party party : allStatePartyWinners) {
            Party oldElem = mapParties.put(party.getPartyName(), party);
            if(oldElem != null){ 
                party.incSeat(oldElem.getSeat());
            }
        }
        //Adding the totaled Party Seats to a List
        allParties = new ArrayList<>(mapParties.values());


        return allParties;
    }

    public List<Person> getPopulation() {
        return this.population;
    }
    public Set<State> getStates() {
        return this.states;
    }
}
