package src.parties;
public class Party {
    
    private String partyName;
    private int seat;
    private int vote;
    /**
     * Creates a Party with a default 0 seats and 0 votes with a party name
     * @param partyName : name of party
     */
    public Party(String partyName){
        this.partyName = partyName;
        this.seat=0;
        this.vote=0;
    }

    /**
     * Increment vote by 1 when a person voted for a party. 
     */
    public void incVote(){
        this.vote++;
    }
    /**
     * Adds vote by the number of votes given.
     * @param vote : Number of votes
     */
    public void addVote(int vote){
        this.vote += vote;
    }
    /**
     * Gets the number of votes for a party
     * @return : number of votes
     */
    public int getVote(){
        return this.vote;
    }
    /** 
     * Sets the number of seats won by party.
     * @param seat : Number of seat won
     */
    public void setSeat(int seat) {
        this.seat = seat;
    }
    /**
     * Increments the number of seats of a party.
     * @param seat
     */
    public void incSeat(int seat){
        this.seat += seat;
    }
    /** 
     * Gets the neam of the Party.
     * @return : name of party
     */
    public String getPartyName() {
        return partyName;
    }
    /**
     * Gets the number of seats the Party has.
     * @return : number of seats
     */
    public int getSeat() {
        return seat;
    }
}
