package src.parties;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import src.countries.Canada;
import src.countries.Country;
import src.countries.Mexico;
import src.countries.USA;

public class Parties {
    /**
     * An enum that has all the american political parties with a name
     */
    public enum USAParties{
        Democratic("Democratic"),Republican("Republican"),Libertarian("Libertarian"),Progressive("Progressive"),Green("Green"),
        Socialist("Socialist"),Liberty("Liberty"),Conservative("Conservative");
        private String name;
        USAParties(String name){
            this.name = name;
        }
        public String getName(){
            return name;
        }
    }
    /**
     * An enum that has all the canadain political parties with a name
     */
    public enum canadianParties{
        Democratic("Democratic"),Republican("Republican"),Liberal("Liberal"),Progressive("Progressive"),Green("Green"),
        Socialist("Socialist"),Liberty("Liberty"),BlocQuebecois("Bloc Quebecois"),Conservative("Conservative");
        private String name;
        canadianParties(String name){
            this.name = name;
        }
        public String getName(){
            return name;
        }
    }
    /**
     * An enum that has all the canadain political parties with a name
     */
    public enum mexicanParties{
        Morena("Morena"),Ecologist("Ecologist Green"),Labor("Labor"),National("National Action"),
        Revolutionary("Institutional Revolutionary"),Democratic("Democratic Revolution"),Progressive("Progressive Social Networks");
        private String name;

        mexicanParties(String name){
            this.name = name;
        }
        public String getName(){
            return name;
        }
    }
    private Set<Party> parties;
    /**
     * Creates a parties object so that takes a country input so that it can create the appropriate states for inputted country
     * @param country
     */
    public Parties(Country country){
        if (country instanceof Canada){
            createCanadianParties();
        }else if(country instanceof USA){
            createUSAParties();
        }else if(country instanceof Mexico){
            createMexicanParties();
        }
    }
    /**
     * Gets the set<Party> of the parties object
     * @return : Set<Party>
     */
    public Set<Party> getPartySet(){
        return this.parties;
    }
    /**
     * Returns a random Party that is present in the Parties set<Party> variable
     * @return random Party in Parties object
     */
    public Party getRandomParty(){
        List<Party> newList = new ArrayList<>(this.parties);
        Collections.shuffle(newList);
        Party randParty = newList.get(0);
        return randParty;
    }
    /**
     * Sets the Set<Party> to the parties object
     * @param parties : Set<Party>
     */
    public void setParties(Set<Party> parties){
        this.parties = parties;
    }
    /**
     * Creates the parties object for mexico
     */
    private void createMexicanParties(){
        Set<Party> Parties = new HashSet<Party>();
        for (mexicanParties party : mexicanParties.values()) {
            Party newParty = new Party(party.getName());
            Parties.add(newParty);
        }
        this.parties = Parties;
    } 
    /**
     * Creates the parties object for canada
     */
    private void createCanadianParties(){
        Set<Party> Parties = new HashSet<Party>();
        for (canadianParties party : canadianParties.values()) {
            Party newParty = new Party(party.getName());
            Parties.add(newParty);
        }
        this.parties = Parties;
    } 
    /**
     * Creates the parties object for USA
     */
    private void createUSAParties(){
        Set<Party> Parties = new HashSet<Party>();
        for (USAParties party : USAParties.values()) {
            Party newParty = new Party(party.getName());
            Parties.add(newParty);
        }
        this.parties = Parties;
    } 
}
