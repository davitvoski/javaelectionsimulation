package src.countries;

import java.util.Set;

import src.parties.Party;

public class State{
    private int seat;
    private String name;
    private Set<Party> allParties;

    /**
     * Intializes a State.
     * @param state : Name of the State.
     * @param seat : Number of seats a state can give to a Political party.
     */
    public State(String name, int seat){
        this.name = name;
        this.seat = seat;
    }
    
    /**
     * Intializes a State with a list of Parties.
     * @param name : Name of the State.
     * @param seat : Number of seats a state can give to a Political party.
     * @param allParties : List of Parites in a Country
     */
    public State(String name, int seat,Set<Party> allParties){
        this(name, seat);
        this.allParties = allParties;
    }

    /**
     * Gets List of parties.
     * @return : List of parties.
     */
    public Set<Party> getAllParties() {
        return this.allParties;
    }


    /**
     * Gets the number of seats.
     * @return : number of seats.
     */
    public int getSeat() {
        return this.seat;
    }

    /**
     * Gets the name of the state
     * @return : the name of the state
     */
    public String getName() {
        return this.name;
    }
    /**
     * Prints the State toString
     */
    @Override
    public String toString() {
        String returnString;
        returnString = this.name + " " + this.seat;
        return returnString;
    }
}
