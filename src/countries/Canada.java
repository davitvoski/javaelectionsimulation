package src.countries;
import java.util.HashSet;
import java.util.Set;

import src.systems.SeatBasedCanada;

public class Canada extends Country {
    /**
     * Creates an enum all states with 2 values, seats and name
     */
    public enum allStates {
        ON(121,"Ontario"),QC(78,"Quebec"),BC(42,"British Columbia"),AB(34,"Alberta"),MB(14,"Manitoba"),SK(14,"Saskatchewan"),
        NS(11,"Nova Scotia"),NB(10,"New Brunswick"),NL(7,"Newfoundland and Labrador"),PE(4,"Prince Edward Island"),NT(1,"Northwest Territories"),
        YK(1,"Yukon"),NU(1,"Nunavut");
        
        private int seats;
        private String name;

        allStates(int seats, String name){
            this.seats = seats;
            this.name = name;
        }

        public String getName(){
            return name;
        }
        public int getSeats(){
            return seats;
        }
    }
    /**
     * Creates an a canada object that implements the country abstract class
     * @param people int
     */
    public Canada(int people) {
        super(people);
        this.name = "Canada";
    }

    /**
     * Sets the states that are in the pubic enum
     */
    @Override
    public void createStates() {
        Set<State> states = new HashSet<State>();
        for (allStates state : allStates.values()) {
            State newState = new State(state.getName(),state.getSeats());
            states.add(newState);
        }
        this.states = states;
    }
    /**
     * sets the seatbasedsystem for canada
     */
    public void setSeatBasedCanada(){
        SeatBasedCanada seatBased = new SeatBasedCanada(this.people, this.states);
        this.seatBased = seatBased;
    }
    /**
     * returns the seatbasedCanada system for canada
     * @return SeatBasedCanada
     */
    public SeatBasedCanada getSeatBasedCanada(){
        return (SeatBasedCanada)this.seatBased;
    }

    
}
