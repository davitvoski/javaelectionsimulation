package src.countries;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import src.parties.Parties;
import src.systems.ISystems;

/**
 * Country
 */
public abstract class Country {
    public enum allStates{};
    protected Parties parties;
    protected List<Person> people;
    protected Set<State> states;
    protected ISystems majority;
    protected ISystems seatBased;
    protected int population;
    protected String name;

    /**
     * Intializes a Country.
     * @param people : List of people living inside the country.
     */
    public Country(int population){
        this.population = population;
        //calls the creation of the states right as the country is constructed so that the user doesnt forget to do it themselves
        createStates();
    } 
    /**
     * Calls the getWinner() of SeatBasedVoting System.
     * getWinner() than computes the winnner according to the algorithm in the method.
     * Returns the winning party.
     * @return : party
     */
    public String seatBasedVoteWinner(){
        return seatBased.calculateWinner().getPartyName();
    }

   /**
     * Calls the getWinner() of MajorityVoteSystem.
     * getWinner() than computes the winnner according to the algorithm in the method.
     * Returns the winning party.
     * @return : party
     */
    public String majorityVoteWinner(){
        return majority.calculateWinner().getPartyName();
    }
    /**
     * Gets the Set of Parties in the country.
     * @return : Set of parties.
     */
    public Parties getParties() {
        return parties;
    }
    /**
     * Gets the List of people in the country.
     * @return : list of people
     */
    public List<Person> getPeople() {
        return people;
    }
    /**
     * Get the Number of people living in the country.
     * @return : number of people.
     */
    public int getPopulation() {
        return population;
    }
    /**
     * Gets the Set of States in the Country.
     * @return : Set of States.
     */
    public Set<State> getStates() {
        return this.states;
    }
    /**
     * sets the Parties object for the country
     * @param party
     */
    public void setParties(Parties party){
        this.parties = party;
    }
    /**
     * Returns a random state used for the random generation
     * @return State
     */
    public State getRandomState(){
        List<State> newList = new ArrayList<>(this.states);
        Collections.shuffle(newList);
        State randState = newList.get(0);
        return randState;
    }
    public abstract void createStates();
    /**
     * sets the list of people to the country
     * @param people : List<Person> 
     */
    public void setPeople(List<Person> people) {
        this.people = people;
    }
    /**
     * sets the Set<State> of states to the country
     * @param states : List<State>
     */
    public void setStates(Set<State> states){
        this.states = states;
    }
    /**
     * gets the seatbased system for a country
     * @return ISystem seatBased
     */
    public ISystems getSeatBased() {
        return seatBased;
    }
}