package src.countries;
import java.util.HashSet;
import java.util.Set;


public class Mexico extends Country {
    /**
     * Creates an enum all states with 2 values, seats and name
     */
    public enum allStates {
        Jalisco("Jalisco"),Monterey("Monterey"),Xalapa("Xalapa"),MexicoCity("Mexico City"),Toluca("Toluca");
        
        private int seats;
        private String name;

        allStates(String name){
            this.seats = 40;
            this.name = name;
        }

        public String getName(){
            return name;
        }
        public int getSeats(){
            return seats;
        }
    }
    /**
     * Creates a Mexico country that extends the Country abstract class
     * @param people in
     */
    public Mexico(int people) {
        super(people);
        this.name = "Mexico";
    }

    /**
     * Sets the states that are in the pubic enum
     */
    @Override
    public void createStates() {
        Set<State> states = new HashSet<State>();
        for (allStates state : allStates.values()) {
            State newState = new State(state.getName(),state.getSeats());
            states.add(newState);
        }
        this.states = states;
    }
}
