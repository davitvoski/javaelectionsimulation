package src.countries;

/**
 * Person
 * This class creates a voter.
 */
public class Person{
    private String name;
    private String stateName;
    private String votedParty;

    /**
     * Constructor
     * @param name
     * @param stateName
     * @param votedParty
     */
    public Person(String name,String stateName,String votedParty){
        this.name = name;
        this.stateName = stateName;
        this.votedParty = votedParty;
    }
    /** 
     * Gets the name.
     * @return :  name
     */
    public String getName() {
        return name;
    }
    /** 
     * Gets the state.
     * @return : state
     */
    public String getStateName() {
        return stateName;
    }
    /** 
     * Gets the Party voted for.
     * @return : Party that was voted
     */
    public String getVotedParty() {
        return votedParty;
    }

}