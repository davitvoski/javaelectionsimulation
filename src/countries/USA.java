package src.countries;
import java.util.HashSet;
import java.util.Set;

import src.systems.SeatBasedUsa;

public class USA extends Country {
    /**
     * Creates an enum all states with 2 values, seats and name
     */
    public enum allStates {
        AL(7,"Alabama"),AK(1,"Alaska"),AZ(9,"Arizona"),AR(4,"Arkansas"),CA(53,"California"),CO(7,"Colorado"),
        CT(5,"Connecticut"),DE(1,"Delaware"),FL(27,"Florida"),GA(14,"Georgia"),HI(2,"Hawaii"),ID(2,"Idaho"),
        IL(18,"Illinois"),IN(9,"Indiania"),IA(4,"Iowa"),KS(4,"Kansas"),KY(6,"Kentucky"),LA(6,"Louisiana"),ME(2,"Maine"),
        MD(8,"Maryland"),MA(9,"Massachusetts"),MI(14,"Michigan"),MN(8,"Minnesota"),MS(4,"Mississippi"),MO(8,"Missouri"),
        MT(1,"Montana"),NE(3,"Nebraska"),NV(4,"Nevada"),NH(2,"New Hampshire"),NJ(12,"New Jersey"),NM(3,"New Mexico"),
        NY(27,"New York"),NC(13,"North Carolina"),ND(1,"North Dakota"),OH(16,"Ohio"),OK(5,"Oklahoma"),OR(5,"Oregon"),
        PA(18,"Pennsylvania"),RI(2,"Rhode Island"),SC(7,"South Carolina"),SD(1,"South Dakota"),TN(9,"Tennessee"),
        TX(36,"Texas"),UT(4,"Utah"),VT(1,"Vermont"),VA(11,"Virginia"),WA(10,"washington"),WV(3,"West Virginia"),
        WI(8,"Wisconsin"),WY(1,"Wyoming");

        private int seats;
        private String name;
        allStates(int seats, String name){
            this.seats = seats;
            this.name = name;
        }
        public String getName(){
            return name;
        }
        public int getSeats(){
            return seats;
        }
    }   
    /**
     * Creates a USA country that extends the country abstract class
     * @param population int
     */ 
    public USA(int population) {
        super(population);
        this.name = "USA";
    }
    /**
     * Overrides the createStates which will be used to calculates new states depending on the file input
     */
    @Override
    public void createStates() {
        Set<State> states = new HashSet<State>();
        for (allStates state : allStates.values()) {
            State newState = new State(state.getName(),state.getSeats());
            states.add(newState);
        }
        this.states = states;
    }
    /**
     * Sets the seatbased system for USA which is seatBasedUSA
     */
    public void setSeatBasedUSA() {
        SeatBasedUsa seatBased = new SeatBasedUsa(this.people, this.states);
        this.seatBased = seatBased;
    }
    /**
     * Returns the SeatBasedUSA for country USA
     * @return SeatBasedUSA
     */
    public SeatBasedUsa getSeatBased(){
        return (SeatBasedUsa)this.seatBased;
    }
}
    