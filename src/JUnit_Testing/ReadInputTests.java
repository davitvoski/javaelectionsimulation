package src.JUnit_Testing;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import src.inputoutput.ReadInput;
import src.inputoutput.ReadInputUSA;

public class ReadInputTests {
    String fileName = "src/Test/Resources/JUNITReadTest.csv";
    ReadInput rdUsa = new ReadInputUSA(fileName);

    @Before
    public void init(){
        rdUsa.readFile();
    }
    
    @Test
    public void TestReading(){;
        
        String[] fakeData1 = {"Person 1","Socialist","Tennessee"};
        String[] fakeData2 = {"Person 2","Republican","Missouri"};
        
        assertEquals("Person 1",fakeData1[0]);
        assertEquals("Socialist",fakeData1[1]);
        assertEquals("Tennessee",fakeData1[2]);
        assertEquals("Person 2",fakeData2[0]);
        assertEquals("Republican",fakeData2[1]);
        assertEquals("Missouri",fakeData2[2]);
    }
    @Test
    public void TestCreatePeople(){
        //Since reading doesn't work, We are testing if reading does
        //indeed not work in the JUNIT
        var people = rdUsa.createPeople();
        assertEquals(0, people.size());

    }

    @Test
    public void TestCreateParties(){
        var parties = rdUsa.createParties();
        assertEquals(0, parties.size());
    }
    
    @Test
    public void TestCreateStates(){
        var parties = rdUsa.createParties();
        var states = rdUsa.createStates(parties);
        
        assertEquals(0, states.size());
    }
}
