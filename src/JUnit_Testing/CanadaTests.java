package src.JUnit_Testing;

import src.countries.Canada;
import src.countries.State;
import src.inputoutput.WriteOutput;
import src.parties.Parties;
import src.parties.Party;
import src.parties.Parties.canadianParties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;



public class CanadaTests{

    //check it over
    @Test
    public void TestFileCreationDeletionOutputVote(){
        String noErr = "";
        try {
         //creates country
        Canada canada = new Canada(10000);
        int pop = canada.getPopulation();
        System.out.println(pop);
        //creates parties from country
        Parties parties1 = new Parties(canada);
        //sets the parties in the country
        canada.setParties(parties1);

        //creates an outputOnj using canada and parties
        WriteOutput outputObj1 = new WriteOutput(canada, parties1,"hello.csv");

        outputObj1.deleteFile();
        outputObj1.createFile();
        outputObj1.generateRandomVote(); 
        
        noErr = "No Errors";
        } catch (Exception e) {
            noErr = "Errors";
        }
        
        assertEquals("No Errors", noErr);

    }

    @Test 
    public void TestParties(){
        Canada canada = new Canada(100);
        Parties canadianParties = new Parties(canada);
        //sets the parties in the country
        canada.setParties(canadianParties);
        Parties Canparty = canada.getParties();
        Set<Party> canParties = Canparty.getPartySet();

        Set<Party> actualParty = new HashSet<Party>();
        for (canadianParties party1 : Parties.canadianParties.values()) {
            Party newParty = new Party(party1.getName());
            actualParty.add(newParty);
        }
        Parties actualParties = new Parties(canada);
        actualParties.setParties(actualParty);

        boolean equalParties = true;
        int count;
        for (Party party1 : actualParty) {
            count=0;
            for (Party party2 : canParties) {
                if (!(party1.getPartyName().equals(party2.getPartyName()))){
                    count++;
                }else{
                    //they are equal so dont do anything
                }
            }
            if(actualParty.size()==count){
                equalParties = false;
            }
        }

        assertEquals(true, equalParties);
    }

    @Test
    public void TestPopulation(){
        Canada canada = new Canada(100);
        int population = canada.getPopulation();
        System.out.println(population);
        assertEquals(100, population);
    }

    @Test
    public void TestProvincesSize(){
        Canada canada = new Canada(100);
        canada.createStates();
        Set<State> countryStates = canada.getStates();

        //compares the size
        assertEquals(13, countryStates.size());
    }

    @Test
    public void TestProvinces(){
        Canada canada = new Canada(100);
        canada.createStates();
        Set<State> CanadaStates = canada.getStates();

        Set<State> states = new HashSet<State>();
        for (Canada.allStates state : Canada.allStates.values()) {
            State newState = new State(state.getName(),state.getSeats());
            states.add(newState);
        }

        boolean SameStates = true;
        int count = 0;
        for (State state1 : CanadaStates) {
            count = 0;
            for (State state2 : states) {
                if (!(state1.getName()).equals(state2.getName()) && !(state1.getSeat()==state2.getSeat())){
                    count++;
                }
                if(count==13){
                    SameStates=false;
                }   
            }
        }
        assertEquals(true, SameStates);
    }

}
