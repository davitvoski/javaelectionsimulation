package src.JUnit_Testing;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import src.countries.State;
import src.countries.USA;
import src.inputoutput.ReadInputUSA;
import src.inputoutput.WriteOutput;
import src.parties.Parties;
import src.parties.Party;
import src.parties.Parties.USAParties;

public class USATests{

        
        @Test
        public void TestWinner(){
            //File cant be read through JUNIT since we havent learned how so should return null.
            USA us = new USA(10000);
            ReadInputUSA RIC1 = new ReadInputUSA("sampledata/USAJavaFX1111");
            RIC1.readFile();
            Set<Party> party1 = RIC1.createParties();
            Parties parties1 = new Parties(us);
            parties1.setParties(party1);
            us.setParties(parties1); 
    
            var people1 = RIC1.createPeople();
            us.setPeople(people1);
            var states1 = RIC1.createStates(party1);
            us.setStates(states1);
    
            us.setSeatBasedUSA();
            assertEquals(null, us.getSeatBased().calculateWinner());
        }

        @Test
        public void TestFileCreationDeletionOutputVote(){
            String noErr = "";
            try {
             //creates country
            USA usa = new USA(10000);
            int pop = usa.getPopulation();
            System.out.println(pop);
            //creates parties from country
            Parties parties1 = new Parties(usa);
            //sets the parties in the country
            usa.setParties(parties1);
    
            //creates an outputOnj using canada and parties
            WriteOutput outputObj1 = new WriteOutput(usa, parties1,"hello.csv");
    
            outputObj1.deleteFile();
            outputObj1.createFile();
            outputObj1.generateRandomVote(); 
            
            noErr = "No Errors";
            } catch (Exception e) {
                noErr = "Errors";
            }
            
            assertEquals("No Errors", noErr);
    
        }
    
        @Test 
        public void TestParties(){
            USA usa = new USA(10000);
            Parties canadianParties = new Parties(usa);
            //sets the parties in the country
            usa.setParties(canadianParties);
            Parties Mexparty = usa.getParties();
            Set<Party> canParties = Mexparty.getPartySet();
    
            Set<Party> actualParty = new HashSet<Party>();
            for (USAParties party1 : Parties.USAParties.values()) {
                Party newParty = new Party(party1.getName());
                actualParty.add(newParty);
            }
            Parties actualParties = new Parties(usa);
            actualParties.setParties(actualParty);
    
            boolean equalParties = true;
            int count;
            for (Party party1 : actualParty) {
                count=0;
                for (Party party2 : canParties) {
                    if (!(party1.getPartyName().equals(party2.getPartyName()))){
                        count++;
                    }else{
                        //they are equal so dont do anything
                    }
                }
                if(actualParty.size()==count){
                    equalParties = false;
                }
            }
    
            assertEquals(true, equalParties);
        }
    
        @Test
        public void TestPopulation(){
            USA usa = new USA(10000);
            int population = usa.getPopulation();
            System.out.println(population);
            assertEquals(10000, population);
        }
    
        @Test
        public void TestStateSize(){
            USA usa = new USA(10000);
            usa.createStates();
            Set<State> countryStates = usa.getStates();
    
            //compares the size
            assertEquals(50, countryStates.size());
        }
    
        @Test
        public void TestStates(){
            USA usa = new USA(10000);
            usa.createStates();
            Set<State> AmericanStates = usa.getStates();
    
            Set<State> states = new HashSet<State>();
            for (USA.allStates state : USA.allStates.values()) {
                State newState = new State(state.getName(),state.getSeats());
                states.add(newState);
            }
    
            boolean SameStates = true;
            int count = 0;
            for (State state1 : AmericanStates) {
                count = 0;
                for (State state2 : states) {
                    if (!(state1.getName()).equals(state2.getName()) && !(state1.getSeat()==state2.getSeat())){
                        count++;
                    }
                    if(count==50){
                        SameStates=false;
                    }   
                }
            }
            assertEquals(true, SameStates);
        }
}

