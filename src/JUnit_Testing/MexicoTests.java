package src.JUnit_Testing;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import src.countries.Mexico;
import src.countries.State;
import src.inputoutput.WriteOutput;
import src.parties.Parties;
import src.parties.Party;
import src.parties.Parties.mexicanParties;

public class MexicoTests {
    //check it over
    @Test
    public void TestFileCreationDeletionOutputVote(){
        String noErr = "";
        try {
         //creates country
        Mexico mexico = new Mexico(10000);
        int pop = mexico.getPopulation();
        System.out.println(pop);
        //creates parties from country
        Parties parties1 = new Parties(mexico);
        //sets the parties in the country
        mexico.setParties(parties1);

        //creates an outputOnj using canada and parties
        WriteOutput outputObj1 = new WriteOutput(mexico, parties1,"hello.csv");

        outputObj1.deleteFile();
        outputObj1.createFile();
        outputObj1.generateRandomVote(); 
        
        noErr = "No Errors";
        } catch (Exception e) {
            noErr = "Errors";
        }
        
        assertEquals("No Errors", noErr);

    }

    @Test 
    public void TestParties(){
        Mexico mexico = new Mexico(10000);
        Parties canadianParties = new Parties(mexico);
        //sets the parties in the country
        mexico.setParties(canadianParties);
        Parties Canparty = mexico.getParties();
        Set<Party> canParties = Canparty.getPartySet();

        Set<Party> actualParty = new HashSet<Party>();
        for (mexicanParties party1 : Parties.mexicanParties.values()) {
            Party newParty = new Party(party1.getName());
            actualParty.add(newParty);
        }
        Parties actualParties = new Parties(mexico);
        actualParties.setParties(actualParty);

        boolean equalParties = true;
        int count;
        for (Party party1 : actualParty) {
            count=0;
            for (Party party2 : canParties) {
                if (!(party1.getPartyName().equals(party2.getPartyName()))){
                    count++;
                }else{
                    //they are equal so dont do anything
                }
            }
            if(actualParty.size()==count){
                equalParties = false;
            }
        }

        assertEquals(true, equalParties);
    }

    @Test
    public void TestPopulation(){
        Mexico mexico = new Mexico(10000);
        int population = mexico.getPopulation();
        System.out.println(population);
        assertEquals(10000, population);
    }

    @Test
    public void TestProvincesSize(){
        Mexico mexico = new Mexico(10000);
        mexico.createStates();
        Set<State> countryStates = mexico.getStates();

        //compares the size
        assertEquals(5, countryStates.size());
    }

    @Test
    public void TestProvinces(){
        Mexico mexico = new Mexico(10000);
        mexico.createStates();
        Set<State> MexicanStates = mexico.getStates();

        Set<State> states = new HashSet<State>();
        for (Mexico.allStates state : Mexico.allStates.values()) {
            State newState = new State(state.getName(),state.getSeats());
            states.add(newState);
        }

        boolean SameStates = true;
        int count = 0;
        for (State state1 : MexicanStates) {
            count = 0;
            for (State state2 : states) {
                if (!(state1.getName()).equals(state2.getName()) && !(state1.getSeat()==state2.getSeat())){
                    count++;
                }
                if(count==5){
                    SameStates=false;
                }   
            }
        }
        assertEquals(true, SameStates);
    }
}
