package src.javafx;

import java.io.IOException;
import java.util.Set;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import src.countries.Canada;
import src.countries.Mexico;
import src.countries.USA;
import src.inputoutput.ReadInputCanada;
import src.inputoutput.ReadInputMexico;
import src.inputoutput.ReadInputUSA;
import src.parties.Parties;
import src.parties.Party;
import src.systems.ISystems;
import src.systems.MajorityVote;

public class inputCountryChoice implements EventHandler<ActionEvent>{
    VBox VBContainer;
    Text intro;
    String country;
    HBox HBButtons;
    String filePath;
    int population;
    /**
     * Creates an inputCountryChoice object that extends the eventHandler
     * @param VBContainer : Vbox container used to place all the javafx objects into
     * @param intro : Text intro that will be used to help the user know whats going on
     * @param HBButtons : HBox that will take care of the buttons
     * @param country : String with the country choice
     * @param filePath : a filepath String that will be used in the reading of the file
     * @param population : a population to do some checking
     */
    public inputCountryChoice(VBox VBContainer,Text intro, HBox HBButtons,String country,String filePath, int population){
        this.VBContainer = VBContainer;
        this.intro = intro;
        this.HBButtons = HBButtons;
        this.country = country;
        this.filePath = filePath;
        this.population = population;
    }
    /**
     * Depending on the chosen country it reads from the file differently and displays the information of the winner
     */
    @Override
    public void handle(ActionEvent event) {
        this.intro.setText("Here are the results of the election for the file that you have entered.");
        this.HBButtons.getChildren().setAll();
        Text TMajorityWinner = new Text();
        Text TSeatBasedWinner = new Text();

        if(this.country.equals("mexico")){
            try {
                String[] winnerInfo = Mexico();
                String winnerParty = winnerInfo[0];
                String winnerVotes = winnerInfo[1];

                TMajorityWinner.setText("The Party who won this election is: "+winnerParty+"\nThey got "+winnerVotes+" votes");
                VBContainer.getChildren().add(TMajorityWinner);
            } catch (Exception error) {
                TMajorityWinner.setText("An error has occured.");
            }
        }else if(this.country.equals("usa")){
            try {
                String[] winnerInfo = USA();
                String winnerParty = winnerInfo[0];
                String winnerVotes = winnerInfo[1];
                String seatBasedWinnerParty = winnerInfo[2];
                String seatBasedWinnerSeats = winnerInfo[3];


                TMajorityWinner.setText("The party who got the most votes is: "+winnerParty+" with "+winnerVotes+" votes");
                TSeatBasedWinner.setText("The party who won the election by seats is: "+seatBasedWinnerParty+" with "+seatBasedWinnerSeats+" seats.");                

                VBContainer.getChildren().addAll(TMajorityWinner,TSeatBasedWinner);
            } catch (Exception error) {
                TMajorityWinner.setText("An error has occured.");
            }
        }else if(this.country.equals("canada")){
            try {
                String[] winnerInfo = Canada();
                String majorityWinnerParty = winnerInfo[0];
                String majorityWinnerVotes = winnerInfo[1];
                String seatBasedWinnerParty = winnerInfo[2];
                String seatBasedWinnerSeats = winnerInfo[3];
                System.out.println("we got here");

                TMajorityWinner.setText("The party who got the most votes is: "+majorityWinnerParty+" with "+majorityWinnerVotes+" votes \n");
                TSeatBasedWinner.setText("The party who won the election by seats is: "+seatBasedWinnerParty+" with "+seatBasedWinnerSeats+" seats.");                

                VBContainer.getChildren().addAll(TMajorityWinner,TSeatBasedWinner);
            } catch (Exception error) {
                TMajorityWinner.setText("An error has occured.");
            }
        }
    }
    /**
     * Reads from the inputted file as a canadian election and calculates the winners and returns an array of the winners
     * @return Stringp[] : position 0 is the party with the most votes, pos 1 is the number of votes they got, pos 2 is the party who won the election with the seats and pos 3 is the number of seats they got. 
     * @throws IOException : Handles IO Exception in the file reading
     */
    private String[] Canada() throws IOException{
        Canada canada = new Canada(this.population);

        //reading the file
        ReadInputCanada RIC = new ReadInputCanada(this.filePath);
        RIC.readFile();
        Set<Party> inputParty = RIC.createParties();
        Parties inputParties = new Parties(canada);

        inputParties.setParties(inputParty);
        var inputPeople = RIC.createPeople();
        var inputStates = RIC.createStates(inputParty);

        canada.setParties(inputParties);
        canada.setPeople(inputPeople);
        canada.setStates(inputStates);
        canada.setSeatBasedCanada();

        //returning the winner
        ISystems MajorityVoteCanada = new MajorityVote(canada.getPeople(),canada.getParties().getPartySet());

        String[] winnerArray = new String[10];
        //calculates the winner by most votes
        var canadaWinner = MajorityVoteCanada.calculateWinner();

        winnerArray[0] = canadaWinner.getPartyName();
        winnerArray[1] = String.valueOf(canadaWinner.getVote());

        //calculates the winner by seats
        winnerArray[2] = canada.getSeatBased().calculateWinner().getPartyName();
        winnerArray[3] = String.valueOf(canada.getSeatBased().calculateWinner().getSeat());
        return winnerArray;
    }
    /**
     * Reads from the inputted file as an american election and calculates the winners and returns an array of the winners
     * @return Stringp[] : position 0 is the party with the most votes, pos 1 is the number of votes they got, pos 2 is the party who won the election with the seats and pos 3 is the number of seats they got. 
     * @throws IOException : Handles IO Exception in the file reading
     */
    private String[] USA() throws IOException{
        USA usa = new USA(this.population);

        //reading the file
        ReadInputUSA RIUSA = new ReadInputUSA(this.filePath);
        RIUSA.readFile();
        Set<Party> inputParty = RIUSA.createParties();
        Parties inputParties = new Parties(usa);

        inputParties.setParties(inputParty);
        var inputPeople = RIUSA.createPeople();
        var inputStates = RIUSA.createStates(inputParty);

        usa.setParties(inputParties);
        usa.setPeople(inputPeople);
        usa.setStates(inputStates);
        usa.setSeatBasedUSA();

        //returning the winner
        ISystems MajorityVoteUSA = new MajorityVote(usa.getPeople(),usa.getParties().getPartySet());
        
        String[] winnerArray = new String[4];

        var USAWinner = MajorityVoteUSA.calculateWinner();

        winnerArray[0] = USAWinner.getPartyName();
        winnerArray[1] = String.valueOf(USAWinner.getVote());
        
        //calculates the winner by seats
        winnerArray[2] = usa.getSeatBased().calculateWinner().getPartyName();
        winnerArray[3] = String.valueOf(usa.getSeatBased().calculateWinner().getSeat());

        return winnerArray;
    }
    /**
     * Reads from the inputted file as a mexican election and calculates the winners and returns an array of the winners
     * @return Stringp[] : position 0 is the party with the most votes, pos 1 is the number of votes they got
     * @throws IOException : In case it runs into a problem in the fileIO
     */
    private String[] Mexico() throws IOException{
        Mexico mexico = new Mexico(this.population);

        //reading the file
        ReadInputMexico RIM = new ReadInputMexico(this.filePath);
        RIM.readFile();
        Set<Party> inputParty = RIM.createParties();
        Parties inputParties = new Parties(mexico);

        inputParties.setParties(inputParty);
        var inputPeople = RIM.createPeople();
        var inputStates = RIM.createStates(inputParty);

        mexico.setParties(inputParties);
        mexico.setPeople(inputPeople);
        mexico.setStates(inputStates);

        //returning the winner
        ISystems MajorityVoteMexico = new MajorityVote(mexico.getPeople(),mexico.getParties().getPartySet());
        
        String[] winnerArray = new String[2];

        var mexicoWinner = MajorityVoteMexico.calculateWinner();

        winnerArray[0] = mexicoWinner.getPartyName();
        winnerArray[1] = String.valueOf(mexicoWinner.getVote());
        return winnerArray;
    }
    
}
