package src.javafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class settingChoice implements EventHandler<ActionEvent>{
    VBox VBContainer;
    Text intro;
    String typeOfSetting;
    HBox HBButtons;
    /**
     * Creates a SettingChoice object that will be used to check if its a random generated file or an inputted file
     * @param VBContainer : VBox that will be used to place all the javafx objects in
     * @param intro : Introduction Text
     * @param HBButtons : Hbox that will be used to place the buttons in
     * @param typeOfSetting : A String with the Setting choice
     */
    public settingChoice(VBox VBContainer,Text intro, HBox HBButtons, String typeOfSetting){
        this.typeOfSetting = typeOfSetting;
        this.VBContainer = VBContainer;
        this.intro = intro;
        this.HBButtons = HBButtons;
    }
    /**
     * The handle checks that if it is a random file then it just goes and asks how many people there will be in their election,
     * If it is an inputted file it will go to the filechecker to make sure that the file is available.
     */
    @Override
    public void handle(ActionEvent e) {

        if (this.typeOfSetting.equals("random")){
            this.intro.setText("Please enter how many people you want there to be in your election (population). \n");
            TextField TFFileName = new TextField();
            Button BTextFile = new Button("Enter");
            Text TIncorrect = new Text();
            VBContainer.getChildren().addAll(TIncorrect);            
            HBButtons.getChildren().setAll(TFFileName,BTextFile);

            BTextFile.setOnAction(new peopleChecker(TFFileName,this.VBContainer,this.intro,HBButtons,TIncorrect,"random"));
        }else{
            this.intro.setText("Please enter how many people are in your inputted election file (population). \n");
            TextField TFFileName = new TextField();
            Button BTextFile = new Button("Enter");
            Text TIncorrect = new Text();
            VBContainer.getChildren().addAll(TIncorrect);            
            HBButtons.getChildren().setAll(TFFileName,BTextFile);

            BTextFile.setOnAction(new peopleChecker(TFFileName,this.VBContainer,this.intro,HBButtons,TIncorrect,"inputted"));
        }
    }
    
}
