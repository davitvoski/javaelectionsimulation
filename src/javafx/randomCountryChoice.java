package src.javafx;

import java.io.IOException;
import java.util.Set;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import src.countries.Canada;
import src.countries.Mexico;
import src.countries.USA;
import src.inputoutput.ReadInputCanada;
import src.inputoutput.ReadInputMexico;
import src.inputoutput.ReadInputUSA;
import src.inputoutput.WriteOutput;
import src.parties.Parties;
import src.parties.Party;
import src.systems.ISystems;
import src.systems.MajorityVote;

public class randomCountryChoice implements EventHandler<ActionEvent>{
    VBox VBContainer;
    Text intro;
    String country;
    HBox HBButtons;
    String filePath;
    int population;
    /**
     * Create a randomCountryChoice object that will create a new election and display its information
     * @param VBContainer : VBox that will be used to place all the javafx objects into
     * @param intro : Text that will be to help the user identify whats going on
     * @param HBButtons : HBox will take care of the buttons
     * @param country : A String of the chosen country either canada, usa, or mexico
     * @param population : int of the population used for the random generation
     */
    public randomCountryChoice(VBox VBContainer,Text intro, HBox HBButtons,String country, int population){
        this.VBContainer = VBContainer;
        this.intro = intro;
        this.HBButtons = HBButtons;
        this.country = country;
        this.population = population;
    }
    /**
     * Depending on the country it will call the correct helper method and create the election,
     * It will then display who the winner was and how many votes or seats they got. The program then stops
     */
    @Override
    public void handle(ActionEvent event) {
        this.intro.setText("Here are the results of the randomly generated elections file.");
        this.HBButtons.getChildren().setAll();
        Text TMajorityWinner = new Text();
        Text TSeatBasedWinner = new Text();

        if(this.country.equals("mexico")){
            try {
                String[] winnerInfo = Mexico();
                String winnerParty = winnerInfo[0];
                String winnerVotes = winnerInfo[1];

                TMajorityWinner.setText("The Party who won this election is: "+winnerParty+"\nThey got "+winnerVotes+" votes");
                VBContainer.getChildren().add(TMajorityWinner);
            } catch (Exception error) {
                TMajorityWinner.setText("An error has occured.");
            }
        }else if(this.country.equals("usa")){
            try {
                String[] winnerInfo = USA();
                String winnerParty = winnerInfo[0];
                String winnerVotes = winnerInfo[1];
                String seatBasedWinnerParty = winnerInfo[2];
                String seatBasedWinnerSeats = winnerInfo[3];


                TMajorityWinner.setText("The party who got the most votes is: "+winnerParty+" with "+winnerVotes+" votes");
                TSeatBasedWinner.setText("The party who won the election by seats is: "+seatBasedWinnerParty+" with "+seatBasedWinnerSeats+" seats.");                

                VBContainer.getChildren().addAll(TMajorityWinner,TSeatBasedWinner);
            } catch (Exception error) {
                TMajorityWinner.setText("An error has occured.");
            }
        }else if(this.country.equals("canada")){
            try {
                String[] winnerInfo = Canada();
                String majorityWinnerParty = winnerInfo[0];
                String majorityWinnerVotes = winnerInfo[1];
                String seatBasedWinnerParty = winnerInfo[2];
                String seatBasedWinnerSeats = winnerInfo[3];
                System.out.println("we got here");

                TMajorityWinner.setText("The party who got the most votes is: "+majorityWinnerParty+" with "+majorityWinnerVotes+" votes \n");
                TSeatBasedWinner.setText("The party who won the election by seats is: "+seatBasedWinnerParty+" with "+seatBasedWinnerSeats+" seats.");                

                VBContainer.getChildren().addAll(TMajorityWinner,TSeatBasedWinner);
            } catch (Exception error) {
                TMajorityWinner.setText("An error has occured.");
            }
        }
    }
    /**
     * Creates new canadian randomized election. It then reads from it and returns the an array with the winner infromation
     * @return Stringp[] : position 0 is the party with the most votes, pos 1 is the number of votes they got, pos 2 is the party who won the election with the seats and pos 3 is the number of seats they got. 
     * @throws IOException : In case it runs into a problem in the fileIO
     */
    private String[] Canada() throws IOException{
        Canada canada = new Canada(this.population);
        Parties outParties = new Parties(canada);
        //writing the file
        String filePath = "generateddata/CanadaJavaFX"+this.population;

        WriteOutput WO = new WriteOutput(canada, outParties,filePath);

        WO.deleteFile();
        WO.createFile();
        WO.generateRandomVote();

        //reading the file
        ReadInputCanada RIC = new ReadInputCanada(filePath);
        RIC.readFile();
        Set<Party> inputParty = RIC.createParties();
        Parties inputParties = new Parties(canada);

        inputParties.setParties(inputParty);
        var inputPeople = RIC.createPeople();
        var inputStates = RIC.createStates(inputParty);

        canada.setParties(inputParties);
        canada.setPeople(inputPeople);
        canada.setStates(inputStates);
        canada.setSeatBasedCanada();

        //returning the winner
        ISystems MajorityVoteCanada = new MajorityVote(canada.getPeople(),canada.getParties().getPartySet());

        String[] winnerArray = new String[10];
        //calculates the winner by most votes
        var canadaWinner = MajorityVoteCanada.calculateWinner();

        winnerArray[0] = canadaWinner.getPartyName();
        winnerArray[1] = String.valueOf(canadaWinner.getVote());

        //calculates the winner by seats
        winnerArray[2] = canada.getSeatBased().calculateWinner().getPartyName();
        winnerArray[3] = String.valueOf(canada.getSeatBased().calculateWinner().getSeat());
        return winnerArray;
    }
        /**
     * Creates new american randomized election. It then reads from it and returns the an array with the winner infromation
     * @return Stringp[] : position 0 is the party with the most votes, pos 1 is the number of votes they got, pos 2 is the party who won the election with the seats and pos 3 is the number of seats they got. 
     * @throws IOException : In case it runs into a problem in the fileIO
     */
    private String[] USA() throws IOException{
        USA usa = new USA(this.population);
        Parties outParties = new Parties(usa);
        //writing the file
        String filePath = "generateddata/USAJavaFX"+this.population;

        WriteOutput WO = new WriteOutput(usa, outParties,filePath);

        WO.deleteFile();
        WO.createFile();
        WO.generateRandomVote();

        //reading the file
        ReadInputUSA RIUSA = new ReadInputUSA(filePath);
        RIUSA.readFile();
        Set<Party> inputParty = RIUSA.createParties();
        Parties inputParties = new Parties(usa);

        inputParties.setParties(inputParty);
        var inputPeople = RIUSA.createPeople();
        var inputStates = RIUSA.createStates(inputParty);

        usa.setParties(inputParties);
        usa.setPeople(inputPeople);
        usa.setStates(inputStates);
        usa.setSeatBasedUSA();

        //returning the winner
        ISystems MajorityVoteUSA = new MajorityVote(usa.getPeople(),usa.getParties().getPartySet());
        
        String[] winnerArray = new String[4];

        var USAWinner = MajorityVoteUSA.calculateWinner();

        winnerArray[0] = USAWinner.getPartyName();
        winnerArray[1] = String.valueOf(USAWinner.getVote());
        
        //calculates the winner by seats
        winnerArray[2] = usa.getSeatBased().calculateWinner().getPartyName();
        winnerArray[3] = String.valueOf(usa.getSeatBased().calculateWinner().getSeat());

        return winnerArray;
    }
    /**
     * Creates new mexican randomized election. It then reads from it and returns the an array with the winner infromation
     * @return Stringp[] : position 0 is the party with the most votes, pos 1 is the number of votes they got
     * @throws IOException : In case it runs into a problem in the fileIO
     */
    private String[] Mexico() throws IOException{
        Mexico mexico = new Mexico(this.population);
        Parties outParties = new Parties(mexico);
        //writing the file
        String filePath = "generateddata/MexicoJavaFX"+this.population;

        WriteOutput WO = new WriteOutput(mexico, outParties,filePath);

        WO.deleteFile();
        WO.createFile();
        WO.generateRandomVote();

        //reading the file
        ReadInputMexico RIM = new ReadInputMexico(filePath);
        RIM.readFile();
        Set<Party> inputParty = RIM.createParties();
        Parties inputParties = new Parties(mexico);

        inputParties.setParties(inputParty);
        var inputPeople = RIM.createPeople();
        var inputStates = RIM.createStates(inputParty);

        mexico.setParties(inputParties);
        mexico.setPeople(inputPeople);
        mexico.setStates(inputStates);

        //returning the winner
        ISystems MajorityVoteMexico = new MajorityVote(mexico.getPeople(),mexico.getParties().getPartySet());
        
        String[] winnerArray = new String[2];

        var mexicoWinner = MajorityVoteMexico.calculateWinner();

        winnerArray[0] = mexicoWinner.getPartyName();
        winnerArray[1] = String.valueOf(mexicoWinner.getVote());
        return winnerArray;
    }
    
}

