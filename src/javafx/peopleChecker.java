package src.javafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class peopleChecker implements EventHandler<ActionEvent>{
    VBox VBContainer;
    Text intro;
    HBox HBButtons;
    TextField TFTextfield;
    Text TIncorrect;
    String typeOfSetting;
    /**
     * 
     * @param TFTextfield : Input textfield
     * @param VBContainer : VBox container that will be used to place all the javafx objects in
     * @param intro : Introduction textfield
     * @param HBButtons : Hbox for the buttons
     * @param TIncorrect : A Text that will displa an error message if the input is incorrect
     * @param typeOfSetting : String of the type of setting they have, random or inputted
     */
    public peopleChecker(TextField TFTextfield,VBox VBContainer,Text intro,HBox HBButtons, Text TIncorrect, String typeOfSetting){
        this.TFTextfield = TFTextfield;
        this.VBContainer = VBContainer;
        this.intro = intro;
        this.HBButtons = HBButtons;
        this.TIncorrect = TIncorrect;
        this.typeOfSetting = typeOfSetting;
    }
    /**
     * The handle method will check and make sure that the input is greater than 1000, if it is less or a double or text, it will
     * write in the TIncorrect text and display that there is an error and they have to retry
     */
    @Override
    public void handle(ActionEvent event) {
        int populationInt = 0;
        try {
            populationInt = Integer.parseInt(this.TFTextfield.getText());
        } catch (Exception e) {
            this.TIncorrect.setText("The number you have entered is not a valid number. Make sure that it is a an int greater than or equal to 1000");
        }

        if (populationInt < 1000){
            this.TIncorrect.setText("Invalid input, try again.");
        }else{
            this.TIncorrect.setText("");
            Button BCanada = new Button("Canada");
            Button BUSA = new Button("USA");
            Button BMexico = new Button("Mexico");
            //clears all the buttons
            HBButtons.getChildren().setAll();

            if (this.typeOfSetting.equals("random")){
                this.intro.setText("Now please select the country you want to randomly generate a vote for.\n");
                //sets the new buttons
                HBButtons.getChildren().setAll(BCanada,BUSA,BMexico);
    
                BCanada.setOnAction(new randomCountryChoice(this.VBContainer,this.intro,HBButtons,"canada",populationInt));
                BUSA.setOnAction(new randomCountryChoice(this.VBContainer,this.intro,HBButtons,"usa",populationInt));
                BMexico.setOnAction(new randomCountryChoice(this.VBContainer,this.intro,HBButtons,"mexico",populationInt));
            }else{
                this.intro.setText("Now please type the file name that has been placed in the sample data folder.\n");
                
                TextField TFFileName = new TextField();
                Button BTextFile = new Button("Enter");
                Text TIncorrect = new Text();
                VBContainer.getChildren().addAll(TIncorrect);            
                HBButtons.getChildren().setAll(TFFileName,BTextFile);
    
                BTextFile.setOnAction(new fileChecker(TFFileName,this.VBContainer,this.intro,HBButtons,TIncorrect,populationInt));
            }
        }

    }
}

