package src.javafx;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class fileChecker implements EventHandler<ActionEvent>{
    VBox VBContainer;
    Text intro;
    HBox HBButtons;
    TextField TFTextfield;
    Text TIncorrect;
    int population;
    /**
     * Creates a a fileChecker object with all the JavaFX that are required to keep the page dynamic and changing
     * @param TFTextfield : The input textfield
     * @param VBContainer : VBox that will be used to placed new javafx objects into
     * @param intro : Introduction text
     * @param HBButtons : HBox for the buttons
     * @param TIncorrect : 
     * @param population
     */
    public fileChecker(TextField TFTextfield,VBox VBContainer,Text intro,HBox HBButtons, Text TIncorrect, int population){
        this.TFTextfield = TFTextfield;
        this.VBContainer = VBContainer;
        this.intro = intro;
        this.HBButtons = HBButtons;
        this.TIncorrect = TIncorrect;
        this.population = population;
    }

    @Override
    public void handle(ActionEvent e) {
        String fileName = this.TFTextfield.getText();
        String filePath = "sampledata/"+fileName;

        File newFile = new File(filePath);

        if (newFile.exists() && !filePath.equals("sampledata/")){
            this.TIncorrect.setText("");
            this.intro.setText("Now please select the country of the inputed file. \n");
            this.HBButtons.getChildren().setAll();

            Button BCanada = new Button("Canada");
            Button BUSA = new Button("USA");
            Button BMexico = new Button("Mexico");
            
            HBButtons.getChildren().setAll(BCanada,BUSA,BMexico);

            BCanada.setOnAction(new inputCountryChoice(this.VBContainer,this.intro,HBButtons,"canada",filePath,this.population));
            BUSA.setOnAction(new inputCountryChoice(this.VBContainer,this.intro,HBButtons,"usa",filePath,this.population));
            BMexico.setOnAction(new inputCountryChoice(this.VBContainer,this.intro,HBButtons,"mexico",filePath,this.population));

        }else{
            this.TIncorrect.setText("The file you have entered cannot be found in the sample data folder. Try Again");
        }

    }
}
