
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import src.javafx.settingChoice;

public class javafxApp extends Application{
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage){
        Group root = new Group();
        //creates the scene
        Scene scene= new Scene(root, 900,600);
        scene.setFill(Color.PINK);

        VBox VBTypeOfElection = new VBox();
        HBox HBTypeChoice = new HBox();

        Text TIntroduction = new Text("Welcome to our election simulator. We have two settings, either you input a file or we generate a random election for you.\n\n If you are inputting a text file make sure that it is located in the sampleData folder in this project. \n\nThe file must follow this format. Person name,Political Party,State \n There must be commas (,) in between the information or else it will not work\n\n EX: RaphaelCanciani,green,quebec \n\n Click the setting that is applicable\n");

        Button BInputFile = new Button("Input File");
        Button BRandomGeneration = new Button("Randomly Generated");

        HBTypeChoice.getChildren().addAll(BInputFile,BRandomGeneration);
        VBTypeOfElection.getChildren().addAll(TIntroduction,HBTypeChoice);

        root.getChildren().setAll(VBTypeOfElection);
        stage.setTitle("Election Simulator");
        stage.setScene(scene);
        stage.show();

        BInputFile.setOnAction(new settingChoice(VBTypeOfElection, TIntroduction, HBTypeChoice, "input"));
        BRandomGeneration.setOnAction(new settingChoice(VBTypeOfElection, TIntroduction, HBTypeChoice, "random"));
    }
}
